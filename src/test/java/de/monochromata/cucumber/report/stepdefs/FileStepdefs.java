package de.monochromata.cucumber.report.stepdefs;

import static de.monochromata.cucumber.report.config.ConfigurationFactory.CONFIG_FILE_PROPERTY;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.write;
import static java.util.Collections.singletonList;
import java.nio.file.Paths;
import cucumber.api.java8.En;
import de.monochromata.cucumber.report.World;

public class FileStepdefs implements En {

    private boolean defaultPropertiesFileCreated = false;
    private boolean customPropertiesFileCreated = false;

    public FileStepdefs(final World world) {
        Given("test output directory $testOutputDir", () -> {
            world.testOutputDir = createTempDirectory("testOutputDir");
            world.testOutputDir.toFile().deleteOnExit();
        });

        Given("properties file in default location containing", (final String contents) -> {
            world.propertiesFile = Paths.get("cucumber-reporting.properties");
            write(world.propertiesFile, singletonList(contents));
            defaultPropertiesFileCreated = true;
        });

        Given("properties file $propertiesFile containing", (final String contents) -> {
            String propertiesFileToWrite = contents;
            world.propertiesFile = createTempFile("propertiesFile", ".properties");
            if((world.trendsStatsFile != null)) {
                propertiesFileToWrite = propertiesFileToWrite.replaceAll("\\$trendsStatsFile", world.trendsStatsFile.toString());
            }
            write(world.propertiesFile, singletonList(propertiesFileToWrite));
            System.setProperty(CONFIG_FILE_PROPERTY, world.propertiesFile.toString());
            customPropertiesFileCreated = true;
        });

        Given("trends stats file $trendsStatsFile containing", (final String contents) -> {
            world.trendsStatsFile = createTempFile("trendsStatsFile", ".json");
            write(world.trendsStatsFile, singletonList(contents));

        });

        Given("a non-existant properties file $propertiesFile", () -> {
            world.propertiesFile = createTempFile("propertiesFile", ".properties");
            world.propertiesFile.toFile().delete();
            System.setProperty(CONFIG_FILE_PROPERTY, world.propertiesFile.toString());
        });

        After(scenario -> {
            if (defaultPropertiesFileCreated) {
                world.propertiesFile.toFile().delete();
                defaultPropertiesFileCreated = false;
            }
            if (customPropertiesFileCreated) {
                System.setProperty(CONFIG_FILE_PROPERTY, "");
                world.propertiesFile.toFile().delete();
            }
        });
    }

}