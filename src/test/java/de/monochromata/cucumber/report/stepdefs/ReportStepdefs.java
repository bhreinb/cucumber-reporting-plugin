package de.monochromata.cucumber.report.stepdefs;

import static java.util.stream.Collectors.joining;
import static org.assertj.core.api.Assertions.assertThat;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import cucumber.api.java8.En;
import de.monochromata.cucumber.report.World;

public class ReportStepdefs implements En {

    public ReportStepdefs(final World world) {
        Then("the test output dir testOutputDir contains an html report for project {string}",
                (final String projectName) -> {
                    final String overviewContents = getFeaturesOverviewContents(world);
                    assertThat(overviewContents).contains("<td>" + projectName + "</td>");
                });

        Then("the test output dir testOutputDir contains an html report for build number {string}",
                (final String buildNumber) -> {
                    final String overviewContents = getFeaturesOverviewContents(world);
                    assertThat(overviewContents).contains("<td>" + buildNumber + "</td>");
                });

        Then("the test output dir testOutputDir contains an html report for classification {string} {string}",
                (final String key, final String value) -> {
                    final String overviewContents = getFeaturesOverviewContents(world);
                    assertThat(overviewContents).contains("<th>" + key + "</th>");
                    assertThat(overviewContents).contains("<td>" + value + "</td>");
                });

        Then("the test output dir testOutputDir contains an html report for tag {string}",
                (final String tag) -> {
                    final String tagsOverviewContents = getTagsOverviewContents(world);
                    assertThat(tagsOverviewContents).containsPattern(String.format("<td class=\"tagname\"><a href=\".*\">%s</a></td>", tag));
                });

    }

    private String getFeaturesOverviewContents(final World world) throws IOException {
        final Path featuresOverview = getFeaturesOverview(world);
        assertThat(featuresOverview).exists();
        final String overviewContents = getFileOutputFromCucumberReporter(featuresOverview);
        return overviewContents;
    }

    private String getTagsOverviewContents(final World world) throws IOException {
        final Path tagsOverview = getTagsOverview(world);
        assertThat(tagsOverview).exists();
        final String overviewContents = getFileOutputFromCucumberReporter(tagsOverview);
        return overviewContents;
    }

    private Path getFeaturesOverview(final World world) {
        final Path path = world.testOutputDir.resolve("cucumber-html-reports").resolve(
                "overview-features.html");
        return path;
    }

    private Path getTagsOverview(final World world) {
        final Path path = world.testOutputDir.resolve("cucumber-html-reports").resolve(
                "overview-tags.html");
        return path;
    }

    private String getFileOutputFromCucumberReporter(final Path path) throws IOException {
        return Files.lines(path, Charset.forName("UTF-8")).collect(
                joining(" "));
    }

}