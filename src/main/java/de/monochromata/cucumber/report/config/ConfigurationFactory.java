package de.monochromata.cucumber.report.config;

import static java.lang.Boolean.parseBoolean;
import static java.util.Optional.ofNullable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Pattern;
import org.apache.commons.lang3.StringUtils;
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.presentation.PresentationMode;
import net.masterthought.cucumber.reducers.ReducingMethod;
import net.masterthought.cucumber.sorting.SortingMethod;

public class ConfigurationFactory {

    private static final String DEFAULT_FILENAME = "cucumber-reporting.properties";
    private static final String PRESENTATION_MODE_PREFIX = "presentationMode.";
    private static final String REDUCING_METHOD_PREFIX = "reducingMethod.";
    private static final String CLASSIFICATIONS_PREFIX = "classifications.";
    public static final String CONFIG_FILE_PROPERTY = "cucumber.reporting.config.file";

    public static Configuration getConfiguration(final File outputDir) {

        final Properties properties = loadProperties();
        final String projectName = properties.getProperty("projectName",
                "No Name (add projectName to cucumber-reporting.properties)");
        final Configuration configuration = new Configuration(outputDir, projectName);
        configureBuildNumber(configuration, properties);
        configureSortingMethod(configuration, properties);
        configureTagsToExcludeFromCharts(configuration, properties);
        configureTrendsStatsFile(configuration, properties);
        configureRepeatedConfigurationKeys(configuration, properties);
        return configuration;

    }

    protected static void configureBuildNumber(final Configuration configuration, final Properties properties) {

        configuration.setBuildNumber(properties.getProperty("buildNumber"));

    }

    protected static void configureSortingMethod(final Configuration configuration, final Properties properties) {

        SortingMethod sortingMethod = SortingMethod.NATURAL;
        final String configSortingMethod = properties.getProperty("sortingMethod");
        if(StringUtils.isNotEmpty(configSortingMethod)) {
            sortingMethod = Enum.valueOf(SortingMethod.class, configSortingMethod);
        }
        configuration.setSortingMethod(sortingMethod);

    }

    protected static void configureTagsToExcludeFromCharts(final Configuration configuration, final Properties properties) {

        final String keyPattern = "tagsToExcludeFromChart";
        List<String> tagsToExclude = getPropertyList(properties, keyPattern);
        configuration.setTagsToExcludeFromChart(tagsToExclude.toArray(new String[0]));

    }

    protected  static void configureTrendsStatsFile(final Configuration configuration, final Properties properties) {

        String trendsStatsFile = properties.getProperty("trendsStatsFile");
        if(StringUtils.isNotEmpty(trendsStatsFile)) {
            configuration.setTrendsStatsFile(new File(trendsStatsFile));
        }

    }

    protected static void configureRepeatedConfigurationKeys(final Configuration configuration, final Properties properties) {

        final Enumeration<Object> keys = properties.keys();
        while (keys.hasMoreElements()) {
            final String qualifiedKey = (String) keys.nextElement();
            if (qualifiedKey.startsWith(PRESENTATION_MODE_PREFIX)) {
                configurePresentationMode(qualifiedKey, configuration, properties);
            } else if (qualifiedKey.startsWith(REDUCING_METHOD_PREFIX)) {
                configureReducingMethod(qualifiedKey,configuration, properties);
            } else if (qualifiedKey.startsWith(CLASSIFICATIONS_PREFIX)) {
                configureClassifications(qualifiedKey,configuration, properties);
            }
        }

    }

    protected static Properties loadProperties() {

        final Properties properties = new Properties();
        final InputStream stream = getPropertiesStream();
        if (stream != null) {
            try {
                properties.load(new InputStreamReader(stream, "UTF-8"));
            } catch (final IOException e) {
                throw new UncheckedIOException(e);
            }
        }
        return properties;

    }

    protected static InputStream getPropertiesStream() {

        final String filename = getPropertiesFilename();
        final File propertiesFile = ofNullable(Paths.get(filename).toFile())
                .filter(File::exists)
                .filter(File::isFile)
                .filter(File::canRead)
                .orElse(null);
        if (propertiesFile == null && !filename.equals(DEFAULT_FILENAME)) {
            throw new UncheckedIOException(
                    new FileNotFoundException("Cucumber reporting properties file " + filename + " was not found"));
        } else if (propertiesFile != null) {
            try {
                return new FileInputStream(propertiesFile);
            } catch (final FileNotFoundException e) {
                throw new UncheckedIOException(e);
            }
        }
        return null;

    }

    protected static String getPropertiesFilename() {

        return System.getProperty(CONFIG_FILE_PROPERTY, DEFAULT_FILENAME);

    }

    private static List<String> getPropertyList(Properties properties, String name)
    {

        List<String> result = new ArrayList<>();
        for (Map.Entry<Object, Object> entry : properties.entrySet())
        {
            if (((String)entry.getKey()).matches("^" + Pattern.quote(name) + "\\.\\d+$"))
            {
                result.add((String) entry.getValue());
            }
        }
        return result;

    }

    private static void configurePresentationMode(final String qualifiedKey, final Configuration configuration, final Properties properties) {

        final PresentationMode presentationMode = Enum.valueOf(PresentationMode.class, qualifiedKey.substring(PRESENTATION_MODE_PREFIX.length()));
        if(parseBoolean(properties.getProperty(qualifiedKey))) {
            configuration.addPresentationModes(presentationMode);
        }

    }

    private static void configureReducingMethod(final String qualifiedKey, final Configuration configuration, final Properties properties) {

        final ReducingMethod reducingMethod = Enum.valueOf(ReducingMethod.class, qualifiedKey.substring(REDUCING_METHOD_PREFIX.length()));
        if(parseBoolean(properties.getProperty(qualifiedKey))) {
            configuration.addReducingMethod(reducingMethod);
        }

    }

    private static void configureClassifications(final String qualifiedKey, final Configuration configuration, final Properties properties) {

        final String key = qualifiedKey.substring(CLASSIFICATIONS_PREFIX.length());
        configuration.addClassifications(key, properties.getProperty(qualifiedKey));

    }

}
